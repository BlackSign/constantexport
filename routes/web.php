<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

route::get('/constant',[
    'uses' => 'ConstantController@index',
    'as' => 'constant.index'
    ]);

route::get('/broadcast/{name}',[
    'uses' => 'BroadcastController@index',
    'as' => 'broadcast.index'
    ]);

route::get('/export',[
    'uses' => 'UsersController@getMethod',
    'as' => 'broadcast.export.get'
    ]);

route::post('/export',[
    'uses' => 'UsersController@postMethod',
    'as' => 'broadcast.export.post'
    ]);