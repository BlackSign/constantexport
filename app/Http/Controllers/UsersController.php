<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\User;

class UsersController extends Controller
{
    //

    function getMethod(){
        // dd(User::all());
        return view('test',[
            'user'=>User::all(),
        ]);
        
    }

    function postMethod(Request $request){
        $record = User::where('id',$request->id)->get();
        return Excel::download(new UsersExport($record), 'usersg.xlsx');
    }
}
