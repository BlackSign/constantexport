<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BroadcastController extends Controller
{
    //
    function index($variable='kosong'){
        event(new \App\Events\eventBroadcast($variable));
        return $variable;
    }
}
