<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Constant\siswaConstant;

class ConstantController extends Controller
{
    //
    function index(){
        $statussiswa = 4;
        if($statussiswa==siswaConstant::SISWA_AKTIF){
            return 'Siswa Aktif';
        }else if($statussiswa==siswaConstant::SISWA_NON_AKTIF){
            return 'Siswa Non Aktif';
        }else if($statussiswa==siswaConstant::SISWA_DO){
            return 'Siswa DO';
        }else{
            return 'tidak diketahui';
        }

    }
}
