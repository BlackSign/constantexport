<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\eventBroadcast;

class cobaBroadcast
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
        // dd('trigger');
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(eventBroadcast $event)
    {
        //
        // dd('sep',$event->message);
        file_put_contents(base_path('public/assets/broadcast/test.json'), json_encode($event->message));
    }
}
